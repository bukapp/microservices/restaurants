const mongoose = require("mongoose")

const schema = mongoose.Schema({
    name: String,
    owner: String,
    address: String,
    schedules: [],
    rate: Number,
    tags: [String],
    image: String

})

const repository = mongoose.model("Restaurants", schema)

export {repository as RestaurantsRepository}