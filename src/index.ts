import express, { Express, Request, Response } from 'express';
import bodyParser from 'body-parser'
import indexRouter from './routes/index';
import mongoConnexion from './connections/mongo';
import jwt from 'jsonwebtoken'
import middleware from './modules/middleware'

const app: Express = express();
const port: Number = 3007;

app.set('jwt', jwt);
app.set('middleware', middleware)
mongoConnexion()
app.use(bodyParser.urlencoded({ extended: false }));          
app.use(bodyParser.json())
app.use('/', indexRouter);
app.listen(port, () => {
    console.log('[server]: The server is running at http://localhost:' + port);
});
